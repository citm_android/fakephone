package ramos.julian.fakephonecitm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PhoneActivity extends AppCompatActivity {

    public TextView numbers;
    private String numbersText ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);
        numbers = (TextView) findViewById(R.id.textView);
        numbers.setText(" ");
    }


    public void writeNumber(View v) {
        Button b = (Button) v;
        if(numbersText.length()<9) {
            numbersText += b.getText();
            numbers.setText(numbersText);
        }
        else Toast.makeText(this, "Introdueix només 9 dígits!",Toast.LENGTH_SHORT).show();

    }

    public void delete(View view) {
        numbersText ="";
        numbers.setText(numbersText);
        //Toast.makeText(this, "Entra",Toast.LENGTH_SHORT).show();

    }

    public void call(View view) {
    if (numbersText.isEmpty()) Toast.makeText(this, "Fes el favor de posar un número!",Toast.LENGTH_SHORT).show();
    else if (numbersText.length()==9)Toast.makeText(this, "Trucant al "+numbersText+" ...",Toast.LENGTH_SHORT).show();
    else Toast.makeText(this, "Has d'introduir 9 dígits!",Toast.LENGTH_SHORT).show();
    }






}









